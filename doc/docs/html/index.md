# HTML

HTML (HyperText Markup Language) est une application du SGML (Standard Generalized Markup Language). 

Il s’agit d’un standard, d’un ensemble de recommandations publié par le W3C (World Wide Web Consortium) http://www.w3.org

## Historique

HTML est un langage dont :

- la création a débuté en 1989, la version 1.0 ayant vu le jour en 1992. 
- La version 5 est la version actuelle, prévue pour remplacer HTML 3.2, HTML 4 et XHTML 1.x

## Les documents HTML

HTML permet de réaliser des documents, en utilisant une syntaxe structurante. Les pages HTML sont des documents statiques.

### La syntaxe HTML

#### Syntaxe
- Page HTML = fichier texte contenant des balises HTML (exemple : &lt;HTML&gt;)
- Les noms de balises sont insensibles à la casse
  
Il existe :

- des balises d’ouverture (exemple : &lt;HTML&gt;)
- des balises de fermeture (exemple : &lt;/HTML&gt;)
- Certaines balises n’ont pas de fermeture, comme par exemple la balise &lt;BR&gt; qui effectue un retour à la ligne.

La mise en forme du code source n’influe pas sur la mise en forme finale de la page HTML, il est donc possible d’indenter le code source.
Exception faite de l’utilisation de la balise &lt;PRE&gt; qui permet de conserver la mise en forme du texte présent entre la balise d’ouverture et la balise de fermeture.

#### Commentaires

un commentaire apparaîtra dans le code source de la page HTML
Un commentaire n’apparaîtra pas dans la représentation de la page HTML sur le navigateur client.

Syntaxe :
Balise d’ouverture de commentaires : « <!-- »
Balise de fermeture de commentaires : « --> »

### Doctypes

//TODO

### Exemple d'un code html trés simple

<script async src="//jsfiddle.net/mplessis/kt9muj2e/5/embed/html,result/"></script>


