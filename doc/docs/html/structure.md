# Structure

Un document html peut être vu comme un arbre de données, composé par un ensemble de balises décrivant ces données.

Voici un schéma pouvant représenter cet arbre

![Structure html](../assets/html/html-structure.svg)

## En-tête 

l'en-tête d'une page html (la balise *&lt;head&gt;*) peu comprendre :

- Une balise de titre &lt;title&gt;
- Des balises de lien
- Des balises de métadonnées (description de la page HTML) &lt;meta&gt;

### La balise &lt;title&gt;

Elle permet de définir le titre de page HTML

### La balise &lt;link&gt;

Elle permet de lier à la page HTML des fichiers :

- CSS
- Javascript 

Exemple : **&lt;link rel="stylesheet" type="text/css" href="theme.css"&gt;**

### La balise &lt;meta&gt;

Elle prend la forme : &lt;meta name="{nom}" content="{contenu}" …&gt;

Où {nom} peut prendre les valeurs :

- author : le contenu précisera l’auteur
	&lt;meta name="author" content="auteur de la page"&gt;
- description : le contenu précisera la description de la ressource
	&lt;meta name="description" content="Description de la page"&gt;
- content-type : le contenu précisera l’encodage de la ressource
	&lt;meta name="content-type" content="text/html; charset=ISO-8859-1"&gt;
- keywords : le contenu précisera les mots-clés associés à la ressource
	&lt;meta name="keywords" content="cours html cnam"&gt;
- Robots : le contenu précisera les instructions pour robots
&lt;meta name="robots" content="nofollow index"&gt;

Lorsque la balise meta contient le nom "robots" (&lt;meta name="robots"&gt;)

Elle permet de gérer le référencement de la page, les valeurs de contenu sont les suivantes :

- follow : autorise les robots à suivre les liens de la page
- nofollow : interdit aux robots de suivre les liens de la page
- index : autorise les robots à indexer la page
- noindex : interdit aux robots d’indexer la page
- all : follow et index
- none : nofollow et noindex

## Corps

Le corps d’un document HTML (la balise *&lt;body&gt;*) contient le contenu du document. Le contenu peut être présenté de différentes manières par un agent utilisateur

Pour un navigateur visuel : le corps du document peut être assimilé à un canevas où apparait :

- Le texte
- Les images
- Les graphiques, …
- 
Pour un agent audio : le même contenu peut être « parlé »

Voici les différentes catégories de balises que peut contenir la balise &lt;body&gt;

![Balises contenues dans &lt;body&gt;](../assets/html/balises-corps.png)